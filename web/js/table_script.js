function edit_row(no)
{
    document.getElementById("save_button"+no).style.display="inline-block";
    document.getElementById("edit_button"+no).style.display="none";

    var name=document.getElementById("name_row"+no);
    var country=document.getElementById("country_row"+no);
    var change=parseInt(country.innerText,10)*-1;
    update_sum(change);

    var name_data=name.innerHTML;
    var country_data=country.innerHTML;

    name.innerHTML="<input type='text' id='name_text"+no+"' value='"+name_data+"'>";
    country.innerHTML="<input type='text' id='country_text"+no+"' value='"+country_data+"'>";
}

function save_row(no)
{
    var name_val=document.getElementById("name_text"+no).value;
    var country_val=document.getElementById("country_text"+no).value;

    document.getElementById("name_row"+no).innerHTML=name_val;
    document.getElementById("country_row"+no).innerHTML=country_val;
    var change = parseInt(document.getElementById("country_row"+no).innerText,10);
    update_sum(change);

    document.getElementById("edit_button"+no).style.display="inline-block";
    document.getElementById("save_button"+no).style.display="none";
}

function delete_row(no)
{
    var change = parseInt(document.getElementById("country_row"+no).innerText,10)*-1;
    update_sum(change);
    document.getElementById("row"+no+"").outerHTML="";
}

function add_row()
{
    var new_name=document.getElementById("new_name").value;
    var new_country=document.getElementById("new_country").value;

    var table=document.getElementById("data_table");
    var table_len=(table.rows.length)-1;
    var row = table.insertRow(table_len).outerHTML="<tr id='row"+table_len+"'><td id='name_row"+table_len+"'>"+new_name+"</td>" +
        "<td id='country_row"+table_len+"'>"+new_country+"</td>" +
        "<td><input type='button' id='edit_button"+table_len+"' value='Edit' class='edit' onclick='edit_row("+table_len+")'> " +
        "<input type='button' id='save_button"+table_len+"' value='Save' class='save' style='display:none' onclick='save_row("+table_len+")'> " +
        "<input type='button' value='Delete' class='delete' onclick='delete_row("+table_len+")'></td></tr>";

    var change = parseInt(document.getElementById("country_row"+table_len+"").innerText,10);
    update_sum(change);
    document.getElementById("new_name").value="";
    document.getElementById("new_country").value="";
}

function update_sum(change){
    var sum = parseInt(document.getElementById("udgift").innerText, 10);
    document.getElementById("udgift").innerText=sum+change;
}