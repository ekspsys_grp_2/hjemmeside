package Filters;
import javax.faces.application.ResourceHandler;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AdminLoginFilter implements Filter{


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession(false);
        String loginURI = "/admin/index.xhtml";
        String failedLoginPath = "/admin/failure.xhtml";

        boolean loggedIn = session != null && session.getAttribute("admin") != null;
        boolean loginRequest = request.getRequestURI().equals(loginURI);
        boolean accessFailedLoginPage = request.getRequestURI().equals(failedLoginPath);

        if (loggedIn || loginRequest || accessFailedLoginPage) {
            filterChain.doFilter(request, response);
        } else {
            response.sendRedirect(loginURI);
        }
    }

    @Override
    public void destroy() {

    }
}
