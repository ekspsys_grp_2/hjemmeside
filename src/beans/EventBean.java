package beans;

import java.util.Date;

public class EventBean {
    private String navn;
    private Date fraDato;
    private Date tilDato;
    private String beskrivelse;

    public EventBean(String navn, Date fraDato, Date tilDato, String beskrivelse){
        this.fraDato = fraDato;
        this.tilDato = tilDato;
        this.navn = navn;
        this.beskrivelse = beskrivelse;
    }

    public String getNavn() {
        return navn;
    }

    public Date getFraDato() {
        return fraDato;
    }

    public Date getTilDato() {
        return tilDato;
    }

    public String getBeskrivelse() {
        return beskrivelse;
    }
}
