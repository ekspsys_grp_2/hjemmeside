package beans;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.ExternalContextFactory;
import javax.faces.context.FacesContext;
import java.io.IOException;

@ManagedBean
@ApplicationScoped
public class VideoLoginBean {

    private String username;
    private String password;

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    public void login() {
        FacesContext context = FacesContext.getCurrentInstance();
        boolean correctlogin = (username.equals("begyndermaj") && password.equals("password"));
        if (correctlogin) {
            context.getExternalContext().getSessionMap().put("video", 1);
        }
        try {
            context.getExternalContext().redirect("/video1.xhtml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}