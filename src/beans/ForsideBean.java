package beans;

import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.types.Event;
import com.restfb.types.Likes;
import com.restfb.types.Page;
import com.restfb.types.User;

import javax.faces.bean.ManagedBean;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


@ManagedBean(name = "forsideBean", eager = true)
public class ForsideBean {
    public ForsideBean(){
        System.out.println("First bean started!");
    }

    private List<EventBean> generateEventList() {
        ArrayList<EventBean> events = new ArrayList<>();

        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, 1);
        Date date1 = c.getTime();
        c.add(Calendar.DATE, 1);
        Date date2 = c.getTime();
        c.add(Calendar.DATE, 1);
        Date date3 = c.getTime();
        c.add(Calendar.DATE, 1);
        Date date4 = c.getTime();

        events.add(new EventBean("Crazy Event", new Date(), date1, "Crazy event beskrivelse"));
        events.add(new EventBean("Crazy Event2", date1, date2, "Crazy event beskrivelse2"));
        events.add(new EventBean("Et andet event", date2, date2, "Et andet slags event"));
        events.add(new EventBean("Kizz shake", date3, date4, "Kom, shake og kizz"));

        return events;
    }

    public List<EventBean> getEvents(){
        List<EventBean> events = generateEventList();
        return events;
    }

    public String getMessage(){
        return "Hello";
    }

}
